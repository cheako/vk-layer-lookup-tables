// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{collections::HashMap, lazy::SyncLazy, panic::catch_unwind, sync::RwLock};

use ash::vk;

pub(crate) static COMMAND_BUFFERS: SyncLazy<RwLock<HashMap<vk::CommandBuffer, ash::Device>>> =
    SyncLazy::new(Default::default);

pub(crate) unsafe extern "system" fn allocate_command_buffers(
    device: vk::Device,
    create_info: *const vk::CommandBufferAllocateInfo,
    p_command_buffers: *mut vk::CommandBuffer,
) -> vk::Result {
    let result = catch_unwind(|| {
        let global = super::DEVICE.read().unwrap();
        let object = global.get(&device).unwrap();
        let device = object.device.clone();

        let hash_map = &mut COMMAND_BUFFERS.write().unwrap();

        device.allocate_command_buffers(&*create_info).map(|x| {
            for (i, command_buffer) in x.into_iter().enumerate() {
                *p_command_buffers.add(i) = command_buffer;
                hash_map.insert(command_buffer, object.device.clone());
            }
            vk::Result::SUCCESS
        })
    });
    assert!(result.is_ok());
    result.unwrap().into_ok_or_err()
}
