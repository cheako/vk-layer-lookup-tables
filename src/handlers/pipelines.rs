// Copyright (C) 2022 Michael Mestnik <cheako@mikemestnik.net>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::lazy::SyncLazy;
use std::panic::catch_unwind;
use std::slice;
use std::sync::Mutex;

use ash::vk;

enum Pipeline {
    Graphics {
        pipeline: vk::Pipeline,
        create_info: super::types::MyGraphicsPipelineCreateInfo,
        shuffle: bool,
        device: ash::Device,
        allocation_callbacks: Option<vk::AllocationCallbacks>,
    },
    Compute(vk::Pipeline),
}

unsafe impl Send for Pipeline {}

impl Pipeline {
    fn get_pipeline(&mut self) -> vk::Pipeline {
        match self {
            Pipeline::Graphics {
                pipeline,
                create_info,
                shuffle,
                device,
                allocation_callbacks,
            } if *shuffle => {
                let allocation_callbacks = allocation_callbacks.as_ref();
                unsafe { device.destroy_pipeline(*pipeline, allocation_callbacks) };

                let create_infos = create_info.get_infos();
                *pipeline = unsafe {
                    device.create_graphics_pipelines(
                        vk::PipelineCache::null(),
                        create_infos,
                        allocation_callbacks,
                    )
                }
                .unwrap()[0];
                *shuffle = false;
                *pipeline
            }
            Pipeline::Graphics { pipeline, .. } => *pipeline,
            Pipeline::Compute(x) => *x,
        }
    }

    unsafe fn destroy(
        self,
        device: &ash::Device,
        allocation_callbacks: Option<&vk::AllocationCallbacks>,
    ) {
        let pipeline = match self {
            Pipeline::Graphics { pipeline, .. } => pipeline,
            Pipeline::Compute(x) => x,
        };
        unsafe { device.destroy_pipeline(pipeline, allocation_callbacks) }
    }
}

static mut CTR: u64 = 0;
struct Pipelines {
    ids: HashMap<u64, Pipeline>,
}

#[allow(clippy::type_complexity)]
static PIPELINES: SyncLazy<Mutex<Pipelines>> = SyncLazy::new(|| {
    Pipelines {
        ids: Default::default(),
    }
    .into()
});

pub(crate) fn shuffle() {
    for x in PIPELINES
        .lock()
        .unwrap()
        .ids
        .values_mut()
        .filter_map(|x| match x {
            Pipeline::Graphics { shuffle, .. } => Some(shuffle),
            Pipeline::Compute(_) => None,
        })
    {
        *x = true;
    }
}

pub(crate) unsafe extern "system" fn create_graphics_pipeline(
    device: vk::Device,
    pipeline_cache: vk::PipelineCache,
    count: u32,
    p_create_info: *const vk::GraphicsPipelineCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_pipelines: *mut vk::Pipeline,
) -> vk::Result {
    let result = catch_unwind(|| {
        assert_eq!(pipeline_cache, vk::PipelineCache::null());
        let object = super::DEVICE.read().unwrap();
        let device = &object.get(&device).unwrap().device;
        let create_infos = unsafe { slice::from_raw_parts(p_create_info, count as _) };
        || -> Result<_, vk::Result> {
            {
                // sleep(Duration::MILLISECOND * 30);
                let create_infos = create_infos
                    .iter()
                    .map(|x| {
                        let mut x = *x;
                        if x.base_pipeline_handle != vk::Pipeline::null() {
                            let mut object = PIPELINES.lock().unwrap();
                            x.base_pipeline_handle = object
                                .ids
                                .get_mut(&vk::Handle::as_raw(x.base_pipeline_handle))
                                .unwrap()
                                .get_pipeline();
                        }
                        x
                    })
                    .collect::<Vec<_>>();
                unsafe {
                    device.create_graphics_pipelines(
                        pipeline_cache,
                        &create_infos,
                        p_allocator.as_ref(),
                    )
                }
            }
            .map_err(|(_, x)| x)
        }()
        .map(|x| {
            for (i, pipeline) in x.into_iter().enumerate() {
                *p_pipelines.add(i) = <vk::Pipeline as vk::Handle>::from_raw(CTR);
                let device = device.clone();
                PIPELINES.lock().unwrap().ids.insert(
                    CTR,
                    Pipeline::Graphics {
                        pipeline,
                        create_info: create_infos[i].into(),
                        shuffle: false,
                        device,
                        allocation_callbacks: p_allocator.as_ref().copied(),
                    },
                );
                CTR += 1;
            }
            vk::Result::SUCCESS
        })
    });
    assert!(result.is_ok());
    result.unwrap().into_ok_or_err()
}

pub(crate) unsafe extern "system" fn create_compute_pipelines(
    device: vk::Device,
    pipeline_lookup_tables: vk::PipelineCache,
    count: u32,
    p_create_info: *const vk::ComputePipelineCreateInfo,
    p_allocator: *const vk::AllocationCallbacks,
    p_pipelines: *mut vk::Pipeline,
) -> vk::Result {
    let result = catch_unwind(|| {
        || -> Result<_, vk::Result> {
            let object = super::DEVICE.read().unwrap();
            let device = &object.get(&device).unwrap().device;
            Ok(device
                .create_compute_pipelines(
                    pipeline_lookup_tables,
                    slice::from_raw_parts(p_create_info, count as _),
                    p_allocator.as_ref(),
                )
                .map_err(|(_, x)| x)?)
        }()
        .map(|x| {
            for (i, pipeline) in x.into_iter().enumerate() {
                *p_pipelines.add(i) = <vk::Pipeline as vk::Handle>::from_raw(CTR);
                PIPELINES
                    .lock()
                    .unwrap()
                    .ids
                    .insert(CTR, Pipeline::Compute(pipeline));
                CTR += 1;
            }
            vk::Result::SUCCESS
        })
    });
    assert!(result.is_ok());
    result.unwrap().into_ok_or_err()
}

pub(crate) unsafe extern "system" fn destroy_pipeline(
    device: vk::Device,
    pipeline: vk::Pipeline,
    p_allocator: *const vk::AllocationCallbacks,
) {
    let result = catch_unwind(|| {
        let mut h = PIPELINES.lock().unwrap();
        let object = super::DEVICE.read().unwrap();
        let device = &object.get(&device).unwrap().device;
        h.ids
            .remove(&vk::Handle::as_raw(pipeline))
            .map(|pipeline| pipeline.destroy(device, p_allocator.as_ref()));
    });
    assert!(result.is_ok());
    result.unwrap()
}

pub(crate) unsafe extern "system" fn cmd_bind_pipeline(
    command_buffer: vk::CommandBuffer,
    pipeline_bind_point: vk::PipelineBindPoint,
    pipeline: vk::Pipeline,
) {
    let object = super::command_buffer::COMMAND_BUFFERS.read().unwrap();
    let device = object.get(&command_buffer).unwrap();

    let mut object = PIPELINES.lock().unwrap();
    let pipeline = object
        .ids
        .get_mut(&vk::Handle::as_raw(pipeline))
        .unwrap()
        .get_pipeline();

    device.cmd_bind_pipeline(command_buffer, pipeline_bind_point, pipeline.clone())
}
